package com.example.scopribg;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import com.google.ar.core.Anchor;
import com.google.ar.core.Frame;
import com.google.ar.core.Pose;
import com.google.ar.core.Session;
import com.google.ar.core.TrackingState;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.core.exceptions.UnavailableException;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.ArSceneView;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ModelRenderable;
import com.google.ar.sceneform.rendering.ViewRenderable;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class PlayActivity extends FragmentActivity {

    private static final int RC_PERMISSIONS = 0x123;
    private boolean installRequested;

    private ArSceneView arSceneView;

    private ModelRenderable foxRenderable;
    private ViewRenderable foxText;

    private Button invio;
    private EditText editText;
    private TextView textview;
    // True once scene is loaded
    private boolean hasFinishedLoading = false;

    // True once the scene has been placed.
    private boolean objectsplaced = false;

    private String answer;
    private String question;
    private AnchorNode anchorNode;


    @Override
    @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})
    // CompletableFuture requires api level 24
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!DemoUtils.checkIsSupportedDeviceOrFinish(this)) {
            // Not a supported device.
            return;
        }

        setContentView(R.layout.activity_play);
        arSceneView = findViewById(R.id.ux_fragment);

        Intent intent = getIntent();
        question = intent.getStringExtra("question");
        answer = intent.getStringExtra("answer");

        invio = findViewById(R.id.invio);
        editText = findViewById(R.id.editText);
        textview = findViewById(R.id.text_view_id);
        invio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String risposta = editText.getText().toString().toLowerCase();

                Intent sendIntent;
                if(risposta.toLowerCase().trim().equals(answer.toLowerCase().trim())) {
                    sendIntent = new Intent(PlayActivity.this, Success.class);
                } else {
                    sendIntent = new Intent(PlayActivity.this, Failure.class);
                }

                sendIntent.putExtra("answer", answer);
                startActivity(sendIntent);
                finish();
            }
        });

        // Build fex models.
        CompletableFuture<ModelRenderable> foxStage =
                ModelRenderable.builder().setSource(this, Uri.parse("model.sfb")).build();


        // Build a renderable from a 2D View.
        CompletableFuture<ViewRenderable> foxTextStage =
                ViewRenderable.builder().setView(this, R.layout.test).build();

        CompletableFuture.allOf(
                foxStage,
                foxTextStage)
                .handle(
                        (notUsed, throwable) -> {

                            if (throwable != null) {
                                DemoUtils.displayError(this, "Unable to load renderable", throwable);
                                return null;
                            }

                            try {
                                foxRenderable = foxStage.get();
                                foxText = foxTextStage.get();

                                // Everything finished loading successfully.
                                hasFinishedLoading = true;

                            } catch (InterruptedException | ExecutionException ex) {
                                DemoUtils.displayError(this, "Unable to load renderable", ex);
                            }

                            return null;
                        });


        // Set an update listener on the Scene that will hide the loading message once a Plane is
        // detected.
        arSceneView
                .getScene()
                .addOnUpdateListener(
                        frameTime -> {

                            Frame frame = arSceneView.getArFrame();
                            if (frame == null) {
                                return;
                            }

                            if (frame.getCamera().getTrackingState() != TrackingState.TRACKING) {
                                return;
                            }

                            if (this.anchorNode == null) {

                                Session session = arSceneView.getSession();

                                float[] position = { 0, -1.5f, -1.75f };       // 75 cm away from camera
                                float[] rotation = { 0, 0, 0, 1f };

                                Anchor anchor =  session.createAnchor(new Pose(position, rotation));

                                anchorNode = new AnchorNode(anchor);
                                anchorNode.setParent(arSceneView.getScene());
                                Node objects = createObjects();
                                anchorNode.addChild(objects);
                                return;
                            }
                        });

        // Lastly request CAMERA permission which is required by ARCore.
        DemoUtils.requestCameraPermission(this, RC_PERMISSIONS);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (arSceneView == null) {
            return;
        }

        if (arSceneView.getSession() == null) {
            // If the session wasn't created yet, don't resume rendering.
            // This can happen if ARCore needs to be updated or permissions are not granted yet.
            try {
                Session session = DemoUtils.createArSession(this, installRequested);
                if (session == null) {
                    installRequested = DemoUtils.hasCameraPermission(this);
                    return;
                } else {
                    arSceneView.setupSession(session);
                }
            } catch (UnavailableException e) {
                DemoUtils.handleSessionException(this, e);
            }
        }

        try {
            arSceneView.resume();
        } catch (CameraNotAvailableException ex) {
            DemoUtils.displayError(this, "Unable to get camera", ex);
            finish();
            return;
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        if (arSceneView != null) {
            arSceneView.pause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (arSceneView != null) {
            arSceneView.destroy();
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, @NonNull String[] permissions, @NonNull int[] results) {
        if (!DemoUtils.hasCameraPermission(this)) {
            if (!DemoUtils.shouldShowRequestPermissionRationale(this)) {
                // Permission denied with checking "Do not ask again".
                DemoUtils.launchPermissionSettings(this);
            } else {
                Toast.makeText(
                        this, "Camera permission is needed to run this application", Toast.LENGTH_LONG)
                        .show();
            }
            finish();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            // Standard Android full-screen functionality.
            getWindow()
                    .getDecorView()
                    .setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }


    private Node createObjects() {
        Node base = new Node();
        Node baseNode = new Node();
        baseNode.setParent(base);
        baseNode.setLocalPosition(new Vector3(0.0f, 0.5f, 0.0f));
        Quaternion q2 = Quaternion.axisAngle(new Vector3(0, 1f, 0f), 120f);
        baseNode.setLocalRotation(q2);
        Node fox = new Node();
        fox.setParent(baseNode);
        fox.setRenderable(foxRenderable);
        fox.setLocalScale(new Vector3(0.5f, 0.5f, 0.5f));
        Quaternion q3 = Quaternion.axisAngle(new Vector3(0, 1f, 0f), 50f);
        Node answern = new Node();
        answern.setParent(baseNode);
        answern.setRenderable(foxText);
        View ftext = foxText.getView();
        TextView tview = ftext.findViewById(R.id.text_view_id);
        tview.setText(question);
        answern.setLocalPosition(new Vector3(0.0f, 0.6f, 0.0f));
        answern.setLocalRotation(q3);


        return base;
    }





}
