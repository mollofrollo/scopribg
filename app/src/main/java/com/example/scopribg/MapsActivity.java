package com.example.scopribg;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private LocationManager locationManager;



    private List<Marker> markers = new ArrayList<>(3);
    private List<Polygon> polygons = new ArrayList<>(3);
    private List<String> questions = new ArrayList<>(3);
    private List<String> answers = new ArrayList<>(3);

    private SharedPreferences pref;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        pref = this.getSharedPreferences("data", MODE_PRIVATE);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

/*        if(pref.getBoolean("Lazzaretto", true)) {
            LatLng accademiaCarrara = new LatLng(45.709774, 9.678923);
            polygons.add(mMap.addPolygon(new PolygonOptions()
                    .add(new LatLng(45.711603, 9.681375),
                            new LatLng(45.709669, 9.674099),
                            new LatLng(45.708406, 9.684418))
                    .strokeColor(Color.GRAY)
                    .fillColor(Color.argb(140, 128, 128, 128))));
            markers.add(mMap.addMarker(new MarkerOptions().position(accademiaCarrara).title("Lazzaretto")));
            questions.add("In quale libro, dopo una serie di peripezie, i protagonisti si reincontrano in un lazzaretto?");
            answers.add("I promessi sposi");
        }*/

        try {
            // fetch JSONArray named users
            InputStream inputStream = getResources().openRawResource(R.raw.jsonfile);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            int ctr;
            try {
                ctr = inputStream.read();
                while (ctr != -1) {
                    byteArrayOutputStream.write(ctr);
                    ctr = inputStream.read();
                }
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONArray array = new JSONArray(byteArrayOutputStream.toString());
            // implement for loop for getting users list data
            for (int i = 0; i < array.length(); i++) {
                List<LatLng> points = new LinkedList<>();
                JSONObject place = array.getJSONObject(i);
                if(pref.getBoolean(place.getString("id"), true)) {
                    double pointlatitude = Double.parseDouble(place.getString("lat"));
                    double pointlongitude = Double.parseDouble(place.getString("lng"));
                    LatLng point = new LatLng(pointlatitude, pointlongitude);
                    markers.add(mMap.addMarker(new MarkerOptions().position(point)
                            .title(place.getString("id"))));
                    questions.add(place.getString("question"));
                    answers.add(place.getString("anwser"));
                    JSONArray polygon = place.getJSONArray("polygon");
                    // fetch mobile number and store it in arraylist
                    for (int j = 0; j < polygon.length(); j++) {
                        JSONObject sppoint = polygon.getJSONObject(j);
                        double latitude = Double.parseDouble(sppoint.getString("lat"));
                        double longitude = Double.parseDouble(sppoint.getString("lng"));
                        LatLng position = new LatLng(latitude, longitude);
                        points.add(position);
                    }
                    points.iterator();
                    polygons.add(mMap.addPolygon(new PolygonOptions()
                            .addAll(points)
                            .strokeColor(Color.GRAY)
                            .fillColor(Color.argb(140, 128, 128, 128))));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(45.705947, 9.679760), 15));
    }

    @SuppressLint("MissingPermission")
    @Override
    public boolean onMarkerClick(Marker click_marker) {
        for (int i = 0; i < markers.size(); i++) {
            Marker list_marker = markers.get(i);
            if (click_marker.equals(list_marker)) {
                Polygon polys = polygons.get(i);

                Location myLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                boolean weAreIn = PolyUtil.containsLocation(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()),
                        polys.getPoints(), true);
                if(weAreIn) {
                    list_marker.setVisible(false);
                    polys.setVisible(false);

                    SharedPreferences.Editor editor = pref.edit();
                    editor.putBoolean(click_marker.getTitle(), false);
                    editor.commit();

                    Intent intent = new Intent(this, PlayActivity.class);
                    intent.putExtra("question", questions.get(i));
                    intent.putExtra("answer", answers.get(i));
                    startActivity(intent);
                } else {
                    Toast msg = Toast.makeText(this, "Hey, puoi accedere a quest'area solo quando sei al suo interno!",
                            Toast.LENGTH_LONG);
                    msg.setGravity(Gravity.CENTER, 0, 0);
                    msg.show();
                }

                return false; // Center the map
            }
        }

        Log.wtf("onMarkerClick", "Marker not found: " + click_marker.getTitle());
        return true;
    }
}
