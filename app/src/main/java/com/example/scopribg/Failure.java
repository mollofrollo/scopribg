package com.example.scopribg;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Failure extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_failure);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView esatto = findViewById(R.id.esatto);
        Intent intent = getIntent();
        String answer = intent.getStringExtra("answer");
        esatto.setText("La risposta giusta era:\n" + answer);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
