# ScopriBG

# Perchè ScopriBG?
ScopriBG nasce con lo scopo di riavvicinare i cittadini, soprattutto i più giovani, ai luoghi storici del comune di Bergamo. Attraverso un semplice ma efficace meccanismo di gioco, aiuta a ricostruire la consapevolezza del territorio che si abita, sfruttando tecnologie all'avanguardia come la realtà aumentata.

# Come si gioca
Per capire come si gioca, guardate [presentazione.pdf](presentazione.pdf) :)

## Prima di compilare il sorgente
Attenzione: prima di compilare importare la propria key per le API di Google Maps in
`values/google_maps_api.xml`.

Verifica che il tuo cellulare supporti Google ARCore.

# Licenza
Il modello 3d della volpe è pubblicato sotto licenza CC-BY da Rachael Hosein (https://poly.google.com/view/biJQtyo7j-r).
